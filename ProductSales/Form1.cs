﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;


namespace ProductSales
{
    public partial class Form1 : Form
    {
        //CSVファイル読み込み用リスト
        List<SaleData> monthSaleList;
        //データグリッドビュー書き込みの正規表現判定用リスト
        List<string> saleBoxList;

        public Form1()
        {
            InitializeComponent();

            monthSaleList = new List<SaleData>();
            //ファイルからデータを読み込むオリジナルメソッド
            ReadFromFile();

            foreach(SaleData data in monthSaleList)
            {
                //DataTableにデータを読み込み
                saleDataSet.saleDataTable.AddsaleDataTableRow(
                    data.Name,data.Month1,data.Month2,data.Month3,
                    data.Month4,data.Month5,data.Month6,data.Month7,
                    data.Month8,data.Month9,data.Month10,data.Month11,data.Month12
                    );
            }
        }

        private void ReadFromFile()
        {
            using (System.IO.StreamReader file = 
                new System.IO.StreamReader(@"..\..\saleData.txt"))
            {
                while (!file.EndOfStream)
                {
                    string line = file.ReadLine();
                    string[] data = line.Split(',');
                    //CSVデータ１行のインスタンスを生成　
                    SaleData saleData = new SaleData(data[0],data[1],
                        data[2],data[3],data[4],data[5],data[6],data[7],
                        data[8],data[9],data[10],data[11],data[12]);
                    this.monthSaleList.Add(saleData);
                }
            }
        }
        private void AddButtonCliecked(object sender, EventArgs e)
        {
            //返値boolのオリジナルメソッドで正規表現判定
            bool result = TextCheck();
            //入力の値が正しかった場合
            if(result == true) {
                //DataTableにデータを追加
                saleDataSet.saleDataTable.AddsaleDataTableRow(
                    this.nameBox.Text,this.saleBox1.Text,this.saleBox2.Text,
                    this.saleBox3.Text,this.saleBox4.Text, this.saleBox5.Text,
                    this.saleBox6.Text, this.saleBox7.Text, this.saleBox8.Text,
                    this.saleBox9.Text, this.saleBox10.Text, this.saleBox11.Text, this.saleBox12.Text
                    );
                //CSVにデータの追加オリジナルメソッド(上書きしない)
                WriteFromFile();
            }
            else
            {
                //エラーメッセージ表示
                MessageBox.Show("月別売上実績は半角整数か空文字で入力して下さい");
            }
        }

        private bool TextCheck()
        {
            saleBoxList = new List<string>();
            saleBoxList.Add(this.saleBox1.Text);
            saleBoxList.Add(this.saleBox2.Text);
            saleBoxList.Add(this.saleBox3.Text);
            saleBoxList.Add(this.saleBox4.Text);
            saleBoxList.Add(this.saleBox5.Text);
            saleBoxList.Add(this.saleBox6.Text);
            saleBoxList.Add(this.saleBox7.Text);
            saleBoxList.Add(this.saleBox8.Text);
            saleBoxList.Add(this.saleBox9.Text);
            saleBoxList.Add(this.saleBox10.Text);
            saleBoxList.Add(this.saleBox11.Text);
            saleBoxList.Add(this.saleBox12.Text);
            for(int i = 0;i < saleBoxList.Count; i++)
            {
                //空文字対応
                if (string.IsNullOrEmpty(saleBoxList[i]))
                {
                    continue;
                }
                //入力の値に数値でない値があった場合falseを返す
                else if (!Regex.IsMatch(saleBoxList[i], @"^[0-9]+$"))
                {
                    return false;
                }
            }           
            return true;
        }

        private void WriteFromFile()
        {
            using (StreamWriter file = File.AppendText(@"..\..\saleData.txt"))
            {
                file.WriteLine(
                    this.nameBox.Text + "," + this.saleBox1.Text + "," + this.saleBox2.Text + "," +
                    this.saleBox3.Text + "," + this.saleBox4.Text + "," + this.saleBox5.Text + "," +
                    this.saleBox6.Text + "," + this.saleBox7.Text + "," + this.saleBox8.Text + "," +
                    this.saleBox9.Text + "," + this.saleBox10.Text + "," + this.saleBox11.Text + "," + this.saleBox12.Text
                    );
            }
        }

        private void ReamoveButtonCliecked(object sender, EventArgs e)
        {
            //データグリッドビューの選択行削除
            int row = this.dataGridView.CurrentRow.Index;
            this.dataGridView.Rows.RemoveAt(row);
        }
    }
}
