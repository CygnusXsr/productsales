﻿namespace ProductSales
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.社員名DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月DataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saleDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.saleDataSet = new ProductSales.SaleDataSet();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.saleBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.saleBox2 = new System.Windows.Forms.TextBox();
            this.saleBox3 = new System.Windows.Forms.TextBox();
            this.saleBox4 = new System.Windows.Forms.TextBox();
            this.saleBox5 = new System.Windows.Forms.TextBox();
            this.saleBox6 = new System.Windows.Forms.TextBox();
            this.saleBox7 = new System.Windows.Forms.TextBox();
            this.saleBox8 = new System.Windows.Forms.TextBox();
            this.saleBox9 = new System.Windows.Forms.TextBox();
            this.saleBox10 = new System.Windows.Forms.TextBox();
            this.saleBox11 = new System.Windows.Forms.TextBox();
            this.saleBox12 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.社員名DataGridViewTextBoxColumn,
            this.月DataGridViewTextBoxColumn,
            this.月DataGridViewTextBoxColumn1,
            this.月DataGridViewTextBoxColumn2,
            this.月DataGridViewTextBoxColumn3,
            this.月DataGridViewTextBoxColumn4,
            this.月DataGridViewTextBoxColumn5,
            this.月DataGridViewTextBoxColumn6,
            this.月DataGridViewTextBoxColumn7,
            this.月DataGridViewTextBoxColumn8,
            this.月DataGridViewTextBoxColumn9,
            this.月DataGridViewTextBoxColumn10,
            this.月DataGridViewTextBoxColumn11});
            this.dataGridView.DataSource = this.saleDataTableBindingSource;
            this.dataGridView.Enabled = false;
            this.dataGridView.Location = new System.Drawing.Point(12, 12);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowTemplate.Height = 21;
            this.dataGridView.Size = new System.Drawing.Size(685, 212);
            this.dataGridView.TabIndex = 0;
            // 
            // 社員名DataGridViewTextBoxColumn
            // 
            this.社員名DataGridViewTextBoxColumn.DataPropertyName = "社員名";
            this.社員名DataGridViewTextBoxColumn.HeaderText = "社員名";
            this.社員名DataGridViewTextBoxColumn.Name = "社員名DataGridViewTextBoxColumn";
            // 
            // 月DataGridViewTextBoxColumn
            // 
            this.月DataGridViewTextBoxColumn.DataPropertyName = "１月";
            this.月DataGridViewTextBoxColumn.HeaderText = "１月";
            this.月DataGridViewTextBoxColumn.Name = "月DataGridViewTextBoxColumn";
            // 
            // 月DataGridViewTextBoxColumn1
            // 
            this.月DataGridViewTextBoxColumn1.DataPropertyName = "２月";
            this.月DataGridViewTextBoxColumn1.HeaderText = "２月";
            this.月DataGridViewTextBoxColumn1.Name = "月DataGridViewTextBoxColumn1";
            // 
            // 月DataGridViewTextBoxColumn2
            // 
            this.月DataGridViewTextBoxColumn2.DataPropertyName = "３月";
            this.月DataGridViewTextBoxColumn2.HeaderText = "３月";
            this.月DataGridViewTextBoxColumn2.Name = "月DataGridViewTextBoxColumn2";
            // 
            // 月DataGridViewTextBoxColumn3
            // 
            this.月DataGridViewTextBoxColumn3.DataPropertyName = "４月";
            this.月DataGridViewTextBoxColumn3.HeaderText = "４月";
            this.月DataGridViewTextBoxColumn3.Name = "月DataGridViewTextBoxColumn3";
            // 
            // 月DataGridViewTextBoxColumn4
            // 
            this.月DataGridViewTextBoxColumn4.DataPropertyName = "５月";
            this.月DataGridViewTextBoxColumn4.HeaderText = "５月";
            this.月DataGridViewTextBoxColumn4.Name = "月DataGridViewTextBoxColumn4";
            // 
            // 月DataGridViewTextBoxColumn5
            // 
            this.月DataGridViewTextBoxColumn5.DataPropertyName = "６月";
            this.月DataGridViewTextBoxColumn5.HeaderText = "６月";
            this.月DataGridViewTextBoxColumn5.Name = "月DataGridViewTextBoxColumn5";
            // 
            // 月DataGridViewTextBoxColumn6
            // 
            this.月DataGridViewTextBoxColumn6.DataPropertyName = "７月";
            this.月DataGridViewTextBoxColumn6.HeaderText = "７月";
            this.月DataGridViewTextBoxColumn6.Name = "月DataGridViewTextBoxColumn6";
            // 
            // 月DataGridViewTextBoxColumn7
            // 
            this.月DataGridViewTextBoxColumn7.DataPropertyName = "８月";
            this.月DataGridViewTextBoxColumn7.HeaderText = "８月";
            this.月DataGridViewTextBoxColumn7.Name = "月DataGridViewTextBoxColumn7";
            // 
            // 月DataGridViewTextBoxColumn8
            // 
            this.月DataGridViewTextBoxColumn8.DataPropertyName = "９月";
            this.月DataGridViewTextBoxColumn8.HeaderText = "９月";
            this.月DataGridViewTextBoxColumn8.Name = "月DataGridViewTextBoxColumn8";
            // 
            // 月DataGridViewTextBoxColumn9
            // 
            this.月DataGridViewTextBoxColumn9.DataPropertyName = "１０月";
            this.月DataGridViewTextBoxColumn9.HeaderText = "１０月";
            this.月DataGridViewTextBoxColumn9.Name = "月DataGridViewTextBoxColumn9";
            // 
            // 月DataGridViewTextBoxColumn10
            // 
            this.月DataGridViewTextBoxColumn10.DataPropertyName = "１１月";
            this.月DataGridViewTextBoxColumn10.HeaderText = "１１月";
            this.月DataGridViewTextBoxColumn10.Name = "月DataGridViewTextBoxColumn10";
            // 
            // 月DataGridViewTextBoxColumn11
            // 
            this.月DataGridViewTextBoxColumn11.DataPropertyName = "１２月";
            this.月DataGridViewTextBoxColumn11.HeaderText = "１２月";
            this.月DataGridViewTextBoxColumn11.Name = "月DataGridViewTextBoxColumn11";
            // 
            // saleDataTableBindingSource
            // 
            this.saleDataTableBindingSource.DataMember = "saleDataTable";
            this.saleDataTableBindingSource.DataSource = this.saleDataSet;
            // 
            // saleDataSet
            // 
            this.saleDataSet.DataSetName = "SaleDataSet";
            this.saleDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "社員名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "１月";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 327);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "２月";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(580, 293);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(89, 41);
            this.addButton.TabIndex = 4;
            this.addButton.Text = "登録";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButtonCliecked);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(580, 366);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(89, 41);
            this.removeButton.TabIndex = 6;
            this.removeButton.Text = "削除";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.ReamoveButtonCliecked);
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(73, 236);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(100, 19);
            this.nameBox.TabIndex = 8;
            // 
            // saleBox1
            // 
            this.saleBox1.Location = new System.Drawing.Point(74, 294);
            this.saleBox1.Name = "saleBox1";
            this.saleBox1.Size = new System.Drawing.Size(100, 19);
            this.saleBox1.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 359);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "３月";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 391);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "４月";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(209, 297);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "５月";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(209, 327);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "６月";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(209, 359);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 12);
            this.label8.TabIndex = 14;
            this.label8.Text = "７月";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(209, 391);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 12);
            this.label9.TabIndex = 15;
            this.label9.Text = "８月";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(399, 297);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 12);
            this.label10.TabIndex = 16;
            this.label10.Text = "９月";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(391, 327);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 12);
            this.label11.TabIndex = 17;
            this.label11.Text = "１０月";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(391, 359);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 12);
            this.label12.TabIndex = 18;
            this.label12.Text = "１１月";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(391, 391);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 12);
            this.label13.TabIndex = 19;
            this.label13.Text = "１２月";
            // 
            // saleBox2
            // 
            this.saleBox2.Location = new System.Drawing.Point(74, 324);
            this.saleBox2.Name = "saleBox2";
            this.saleBox2.Size = new System.Drawing.Size(100, 19);
            this.saleBox2.TabIndex = 20;
            // 
            // saleBox3
            // 
            this.saleBox3.Location = new System.Drawing.Point(74, 356);
            this.saleBox3.Name = "saleBox3";
            this.saleBox3.Size = new System.Drawing.Size(100, 19);
            this.saleBox3.TabIndex = 21;
            // 
            // saleBox4
            // 
            this.saleBox4.Location = new System.Drawing.Point(74, 388);
            this.saleBox4.Name = "saleBox4";
            this.saleBox4.Size = new System.Drawing.Size(100, 19);
            this.saleBox4.TabIndex = 22;
            // 
            // saleBox5
            // 
            this.saleBox5.Location = new System.Drawing.Point(258, 293);
            this.saleBox5.Name = "saleBox5";
            this.saleBox5.Size = new System.Drawing.Size(100, 19);
            this.saleBox5.TabIndex = 23;
            // 
            // saleBox6
            // 
            this.saleBox6.Location = new System.Drawing.Point(258, 324);
            this.saleBox6.Name = "saleBox6";
            this.saleBox6.Size = new System.Drawing.Size(100, 19);
            this.saleBox6.TabIndex = 24;
            // 
            // saleBox7
            // 
            this.saleBox7.Location = new System.Drawing.Point(258, 356);
            this.saleBox7.Name = "saleBox7";
            this.saleBox7.Size = new System.Drawing.Size(100, 19);
            this.saleBox7.TabIndex = 25;
            // 
            // saleBox8
            // 
            this.saleBox8.Location = new System.Drawing.Point(258, 387);
            this.saleBox8.Name = "saleBox8";
            this.saleBox8.Size = new System.Drawing.Size(100, 19);
            this.saleBox8.TabIndex = 26;
            // 
            // saleBox9
            // 
            this.saleBox9.Location = new System.Drawing.Point(447, 293);
            this.saleBox9.Name = "saleBox9";
            this.saleBox9.Size = new System.Drawing.Size(100, 19);
            this.saleBox9.TabIndex = 27;
            // 
            // saleBox10
            // 
            this.saleBox10.Location = new System.Drawing.Point(447, 324);
            this.saleBox10.Name = "saleBox10";
            this.saleBox10.Size = new System.Drawing.Size(100, 19);
            this.saleBox10.TabIndex = 28;
            // 
            // saleBox11
            // 
            this.saleBox11.Location = new System.Drawing.Point(447, 356);
            this.saleBox11.Name = "saleBox11";
            this.saleBox11.Size = new System.Drawing.Size(100, 19);
            this.saleBox11.TabIndex = 29;
            // 
            // saleBox12
            // 
            this.saleBox12.Location = new System.Drawing.Point(447, 388);
            this.saleBox12.Name = "saleBox12";
            this.saleBox12.Size = new System.Drawing.Size(100, 19);
            this.saleBox12.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(55, 297);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 31;
            this.label14.Text = "￥";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(55, 327);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 32;
            this.label15.Text = "￥";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(55, 359);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 33;
            this.label16.Text = "￥";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(55, 391);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 34;
            this.label17.Text = "￥";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(240, 296);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 35;
            this.label18.Text = "￥";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(240, 327);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 36;
            this.label19.Text = "￥";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(240, 359);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 12);
            this.label20.TabIndex = 37;
            this.label20.Text = "￥";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(240, 391);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 38;
            this.label21.Text = "￥";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(430, 297);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 12);
            this.label22.TabIndex = 39;
            this.label22.Text = "￥";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(430, 327);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 12);
            this.label23.TabIndex = 40;
            this.label23.Text = "￥";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(430, 359);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 41;
            this.label24.Text = "￥";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(430, 390);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 12);
            this.label25.TabIndex = 42;
            this.label25.Text = "￥";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(16, 273);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 12);
            this.label26.TabIndex = 43;
            this.label26.Text = "月別売上実績";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 416);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.saleBox12);
            this.Controls.Add(this.saleBox11);
            this.Controls.Add(this.saleBox10);
            this.Controls.Add(this.saleBox9);
            this.Controls.Add(this.saleBox8);
            this.Controls.Add(this.saleBox7);
            this.Controls.Add(this.saleBox6);
            this.Controls.Add(this.saleBox5);
            this.Controls.Add(this.saleBox4);
            this.Controls.Add(this.saleBox3);
            this.Controls.Add(this.saleBox2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.saleBox1);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox saleBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 社員名DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月DataGridViewTextBoxColumn11;
        private System.Windows.Forms.BindingSource saleDataTableBindingSource;
        private SaleDataSet saleDataSet;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox saleBox2;
        private System.Windows.Forms.TextBox saleBox3;
        private System.Windows.Forms.TextBox saleBox4;
        private System.Windows.Forms.TextBox saleBox5;
        private System.Windows.Forms.TextBox saleBox6;
        private System.Windows.Forms.TextBox saleBox7;
        private System.Windows.Forms.TextBox saleBox8;
        private System.Windows.Forms.TextBox saleBox9;
        private System.Windows.Forms.TextBox saleBox10;
        private System.Windows.Forms.TextBox saleBox11;
        private System.Windows.Forms.TextBox saleBox12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
    }
}

