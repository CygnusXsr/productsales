﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductSales
{
    class SaleData
    {
        private string name;
        private string month1;
        private string month2;
        private string month3;
        private string month4;
        private string month5;
        private string month6;
        private string month7;
        private string month8;
        private string month9;
        private string month10;
        private string month11;
        private string month12;

        public SaleData(string name, string month1, string month2, string month3, string month4, string month5, string month6, string month7, string month8, string month9, string month10, string month11, string month12)
        {
            Name = name;
            Month1 = month1;
            Month2 = month2;
            Month3 = month3;
            Month4 = month4;
            Month5 = month5;
            Month6 = month6;
            Month7 = month7;
            Month8 = month8;
            Month9 = month9;
            Month10 = month10;
            Month11 = month11;
            Month12 = month12;
            
        }

        public string Name { get => name; set => name = value; }
        public string Month1 { get => month1; set => month1 = value; }
        public string Month2 { get => month2; set => month2 = value; }
        public string Month3 { get => month3; set => month3 = value; }
        public string Month4 { get => month4; set => month4 = value; }
        public string Month5 { get => month5; set => month5 = value; }
        public string Month6 { get => month6; set => month6 = value; }
        public string Month7 { get => month7; set => month7 = value; }
        public string Month8 { get => month8; set => month8 = value; }
        public string Month9 { get => month9; set => month9 = value; }
        public string Month10 { get => month10; set => month10 = value; }
        public string Month11 { get => month11; set => month11 = value; }
        public string Month12 { get => month12; set => month12 = value; }
    }

}
